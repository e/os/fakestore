/*
 * Copyright 2013-2022 microG Project Team
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.android.play.core.splitinstall.protocol;

import com.google.android.play.core.splitinstall.protocol.ISplitInstallServiceCallback;

interface ISplitInstallService {

    // Method not identified yet
    void a();

    void startInstall(String str, in List<Bundle> list, in Bundle bundle, in ISplitInstallServiceCallback callback);

    // Method not identified yet
    void c(String str);

    // Method not identified yet
    void d(String str);

    // Method not identified yet
    void e(String str);

    void getSessionStates(String str, in ISplitInstallServiceCallback callback);

    // Method not identified yet
    void g(String str);

    // Method not identified yet
    void h(String str, in ISplitInstallServiceCallback callback);

    // Method not identified yet
    void i(String str);

    // Method not identified yet
    void j(String str);

    // Method not identified yet
    void k(String str);

    // Method not identified yet
    void l(String str);

    // Method not identified yet
    void m(String str);
}